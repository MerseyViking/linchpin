# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

import os
import sys
import logging

from flask import Flask, request, render_template, session
from flask.ext import breadcrumbs
from flask.ext import menu
from flask.ext.babel import Babel
from flask.ext.googlemaps import GoogleMaps
from flask.ext.mail import Mail
from flask.ext.navigation import Navigation
from flask.ext.sqlalchemy import SQLAlchemy
import flask.ext.login
from flask.ext.login import LoginManager
from flask.ext.security import Security, SQLAlchemyUserDatastore


# Uses the ISO8601 date format, with the optional 'T' character, and a '.' as the decimal separator.
class LogFormatter(logging.Formatter):
    default_format = logging._STYLES['{'][0]('{asctime}.{msecs:03.0f}Z|{levelname}|{message}')
    debug_format = logging._STYLES['{'][0]('{asctime}.{msecs:03.0f}Z|{levelname}|'
                                           '[{pathname}:{funcName}:{lineno}]|{message}')

    def format(self, record):
        self._style = self.default_format
        self.datefmt = '%Y-%m-%dT%H:%M:%S'

        if record.levelno == logging.DEBUG:
            self._style = self.debug_format

        try:
            if flask.ext.login.current_user.is_anonymous():
                user_name = 'Anonymous'
            else:
                user_name = flask.ext.login.current_user

            record.msg = '{0}|{1}|{2}'.format(request.remote_addr, user_name, record.msg)
        except (AttributeError, RuntimeError):
            # Ignore the request object if we're not logging a request.
            pass

        return logging.Formatter.format(self, record)

logging.basicConfig(level=logging.INFO)

# Create our own logger so that we can separate out our log messages from Flask's.
logger = logging.getLogger('linchpin')
logger.setLevel(logging.INFO)

for handler in logging.root.handlers:
    handler.setFormatter(LogFormatter())

logger.info('Creating Flask application "{}".'.format(__name__.split('.')[0]))
app = Flask(__name__.split('.')[0])

# TODO: Separate out application configuration and site-specific configuration cleanly.
config_class = os.getenv('LINCHPIN_DEFAULT_CONFIG', 'linchpin.config.ProductionConfig')
logger.info('Using configuration settings "{}"'.format(config_class))

app.config.from_object(config_class)
app.config.from_pyfile('security.conf')
app.config.from_envvar('LINCHPIN_USER_CONFIG', silent=True)

log_level = app.config.get('LOG_LEVEL', 'WARNING')
numeric_level = getattr(logging, log_level.upper(), None)

if isinstance(numeric_level, int):
    logger.setLevel(numeric_level)
    logger.info('Switching logging level to {}.'.format(log_level))

# The secret key is used for CSRF handling, and other crypto stuff used by Flask.
if app.config.get('SECRET_KEY', None) is None:
    logger.info('Application secret key is not defined in config. Trying to load from file...')
    try:
        app.config['SECRET_KEY'] = open('linchpin/.secret', 'rb').read()
    except IOError:
        logger.error('Unable to find a secret key for this application. Terminating.')
        sys.exit(1)

logger.info('Application secret key found.')

db = SQLAlchemy(app)
babel = Babel(app)
mail = Mail(app)
nav = Navigation(app)
GoogleMaps(app)
menu.Menu(app=app)
breadcrumbs.Breadcrumbs(app=app)


@babel.localeselector
def get_locale():
    override = request.args.get('lang')

    if override:
        session['lang'] = override

    rv = session.get('lang', 'en')
    return rv

from linchpin.dashboard.views import dashboard_blueprint

app.register_blueprint(dashboard_blueprint)

# TODO: There appears to be no way of redirecting from the password reset page to another.
# Look into overriding the default view, forgot_password().
from linchpin.models.security import User, Role
user_datastore = SQLAlchemyUserDatastore(db, User, Role)

security = Security(app, user_datastore)

from linchpin.models.linchpin import Organization, Project, OrganizationsProjects, PrimeOrganization

db.create_all()


# The initial user will have admin rights, but will be forced to change the password.
@app.before_first_request
def create_user():
    user_datastore.find_or_create_role(name='administrator',
                                       description='Special role able to create and remove users, '
                                                   'and has the rights of all other roles.')

    db.session.commit()

    if User.query.filter().count() == 0:
        logger.info('No users found in database. Creating default admin user.')
        user_datastore.create_user(name='admin', email='admin@example.com', password='admin', roles=['administrator'])
        db.session.commit()

    if Organization.query.filter().count() == 0:
        org = Organization('The Roman Gask Project')
        p = PrimeOrganization(org_id=org)
        db.session.add(p)

        org2 = Organization('Vegitius\' Dodgy Dealers')
        proj1 = Project('Inchtuthil')
        proj1.bounding_box = 'MULTIPOINT((-3.445 56.548), (-3.412 56.534))'
        proj2 = Project('Woodhead')
        proj2.bounding_box = 'MULTIPOINT((-3.383 56.489), (-3.359 56.480))'
        proj3 = Project('Inchtuthil phase II')
        proj3.bounding_box = 'MULTIPOINT((-3.445 56.548), (-3.412 56.534))'
        a = OrganizationsProjects(role='Coordination', organization=org)
        proj1.organizations.append(a)
        b = OrganizationsProjects(role='Slacking', project=proj1)
        org2.projects.append(b)
        # org.projects.append(proj1)
        # org2.projects.append(proj1)
        # org.projects.append(proj2)
        # org.projects.append(proj3)
        proj1.linked_projects = [proj3]
        db.session.add(org)
        db.session.add(org2)
        db.session.add(proj1)
        db.session.add(proj2)
        db.session.commit()


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')