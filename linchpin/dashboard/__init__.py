# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from linchpin import nav

nav.Bar('side', [
    nav.Item('Overview', 'dashboard.overview'),
    nav.Item('Organizations', 'dashboard.organizations'),
    nav.Item('Projects', 'dashboard.projects'),
])