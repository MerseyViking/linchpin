# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from flask import Blueprint, render_template, g, jsonify, request
from flask_security import login_required, current_user, http_auth_required
from flask.ext import breadcrumbs

from linchpin import app
from linchpin.models.linchpin import Organization, Project
from linchpin.dashboard import tables
import linchpin.util as util

dashboard_blueprint = Blueprint('dashboard', __name__, template_folder='templates', static_folder='static')
# breadcrumbs.default_breadcrumb_root(app, '.overview')


def view_organization_dlc():
    """
    Returns the current organziation for breadcrumb goodness.

    :return: A list of dicts with text and url keys.
    """
    org_id = request.view_args['org_id']
    org = Organization.query.filter(Organization.slug == org_id).one()
    return [{'text': org.name, 'url': org.slug}]


def view_project_dlc():
    """
    Returns the current project for breadcrumb goodness.

    :return: A list of dicts with text and url keys.
    """
    project_id = request.view_args['project_id']
    project_date = request.view_args['project_date']
    proj = Project.query.filter(Project.slug == project_id and Project.created_slug == project_date).one()
    return [{'text': proj.name, 'url': proj.created_slug + '/' + proj.slug}]


@dashboard_blueprint.before_request
def before_request():
    g.current_user = current_user


@dashboard_blueprint.route('/overview', methods=['GET'])
@breadcrumbs.register_breadcrumb(dashboard_blueprint, '.', 'Overview')
def overview():
    if util.request_wants_json():
        return overview_json()
    else:
        return overview_html()


@http_auth_required
def overview_json():
    org_count = Organization.query.count()
    proj_count = Project.query.count()

    return jsonify({'organization_count': org_count, 'project_count': proj_count})


@login_required
def overview_html():
    org_count = Organization.query.count()
    proj_count = Project.query.count()

    return render_template('overview.html', org_count=org_count, proj_count=proj_count)


@dashboard_blueprint.route('/organizations', methods=['GET'])
@breadcrumbs.register_breadcrumb(dashboard_blueprint, '.organizations', 'Organizations')
@login_required
def organizations():
    orgs = Organization.query.order_by(Organization.name.asc()).all()
    table = tables.OrganizationsTable(orgs)

    return render_template('organizations.html', org_table=table)


@dashboard_blueprint.route('/organization/<string:org_id>', methods=['GET'])
@breadcrumbs.register_breadcrumb(dashboard_blueprint, '.organizations.organization', '',
                                 dynamic_list_constructor=view_organization_dlc)
@login_required
def organization(org_id):
    org = Organization.query.filter(Organization.slug == org_id).one()
    table = tables.OrganiationProjectSummaryTable(org.projects)
    return render_template('organization.html', organization=org, table=table)


@dashboard_blueprint.route('/projects', methods=['GET'])
@breadcrumbs.register_breadcrumb(dashboard_blueprint, '.projects', 'Projects')
@login_required
def projects():
    proj = Project.query.order_by(Project.name.asc()).all()
    table = tables.ProjectsTable(proj)

    return render_template('projects.html', table=table)


@dashboard_blueprint.route('/project/<string:project_date>/<string:project_id>', methods=['GET'])
@breadcrumbs.register_breadcrumb(dashboard_blueprint, '.projects.project', '',
                                 dynamic_list_constructor=view_project_dlc)
@login_required
def project(project_date, project_id):
    proj = Project.query.filter(Project.slug == project_id and Project.created_slug == project_date).one()
    org_table = tables.OrganizationSummaryTable(proj.organizations)
    linked_proj_table = tables.LinkedProjectSummaryTable(proj.linked_projects + proj.linked_from_projects)
    return render_template('project.html', project=proj, org_table=org_table, linked_proj_table=linked_proj_table)
