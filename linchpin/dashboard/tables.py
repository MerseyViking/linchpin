# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from flask_table import Table, Col, LinkCol


class TitleCol(Col):
    """
    A custom column that formats its contents to use title case.
    """
    def td_format(self, content):
        return content.title()


class ListLinkCol(LinkCol):
    def __init__(self, name, endpoint, sub_attrs, **kwargs):
        super().__init__(name, endpoint, **kwargs)
        self.sub_attrs = sub_attrs

    def td_contents(self, item, attr_list):
        sub_items = getattr(item, attr_list[0])
        contents = list()

        for sub_item in sub_items:
            for sub_attr in self.sub_attrs:
                contents.append(super().td_contents(sub_item, sub_attr.split('.')))

        html = '<ul class="list-inline"><li>'
        html += '</li> | <li>'.join(contents)
        html += '</li></ul>'

        return html


class OrganizationsTable(Table):
    allow_sort = False
    classes = ('table', 'table-hover')

    def sort_url(self, col_id, reverse=False):
        pass

    name = LinkCol('Name', 'dashboard.organization', url_kwargs=dict(org_id='slug'), attr='name')
    project_count = Col('Number of projects')


class OrganiationProjectSummaryTable(Table):
    allow_sort = False
    classes = ('table', 'table-hover')

    def sort_url(self, col_id, reverse=False):
        pass

    name = LinkCol('Project', 'dashboard.project', attr='project.name',
                   url_kwargs=dict(project_date='project.created_slug', project_id='project.slug'))
    status = TitleCol('Publication Status', attr='project.publish_status')


# TODO: This class is the same as OrganizationProjectSummaryTable, but for linked projects which don't have an
# association table class, and so doesn't need the extra level of indirection. But it would be nice to combine the two
# somehow.
class LinkedProjectSummaryTable(Table):
    allow_sort = False
    classes = ('table', 'table-hover')

    def sort_url(self, col_id, reverse=False):
        pass

    name = LinkCol('Project', 'dashboard.project', attr='name',
                   url_kwargs=dict(project_date='created_slug', project_id='slug'))
    status = TitleCol('Publication Status', attr='publish_status')


class OrganizationSummaryTable(Table):
    allow_sort = False
    classes = ('table', 'table-hover')

    def sort_url(self, col_id, reverse=False):
        pass

    name = LinkCol('Organization', 'dashboard.organization', attr='organization.name',
                   url_kwargs=dict(org_id='organization.slug'))
    role = TitleCol('Role', attr='role')


class ProjectsTable(Table):
    allow_sort = False
    classes = ('table', 'table-hover')

    def sort_url(self, col_id, reverse=False):
        pass

    name = LinkCol('Project Name', 'dashboard.project', attr='name',
                   url_kwargs=dict(project_date='created_slug', project_id='slug'))
    status = TitleCol('Publication Status', attr='publish_status')
    org = ListLinkCol('Organisations', 'dashboard.organization', url_kwargs=dict(org_id='organization.slug'),
                      attr='organizations', sub_attrs=('organization.name', ))