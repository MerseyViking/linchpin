# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
from datetime import datetime
from sqlalchemy import event, CheckConstraint, ForeignKey
from sqlalchemy.orm import Session
from slugify import slugify_unicode
from geoalchemy2 import Geography
from sqlalchemy.sql.ddl import AddConstraint

from linchpin import db


projects_projects = db.Table('projects_projects',
                             db.Column('project1_id', db.Integer(), db.ForeignKey('projects.id')),
                             db.Column('project2_id', db.Integer(), db.ForeignKey('projects.id')))


class OrganizationsProjects(db.Model):
    __tablename__ = 'organizations_projects'

    org_id = db.Column('organization_id', db.Integer(), db.ForeignKey('organizations.id'), primary_key=True)
    proj_id = db.Column('project_id', db.Integer(), db.ForeignKey('projects.id'), primary_key=True)
    role = db.Column(db.String)
    organization = db.relationship('Organization', backref='projects')


# Note: Linchpin is not a contacts management system, so we keep Organizations simple for now.
class Organization(db.Model):
    __tablename__ = 'organizations'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, index=True)
    slug = db.Column(db.String, nullable=False, index=True)
    address_1 = db.Column(db.String)
    address_2 = db.Column(db.String)
    address_3 = db.Column(db.String)
    address_4 = db.Column(db.String)
    address_5 = db.Column(db.String)
    address_6 = db.Column(db.String)
    # TODO: Validate against python-iso3166. Or maybe do it on the client side. Or both.
    country = db.Column(db.String)
    postal_code = db.Column(db.String)
    contact_name = db.Column(db.String)
    contact_phone = db.Column(db.String)
    contact_email = db.Column(db.String)
    icon = db.Column(db.String)

    def __init__(self, name):
        self.name = name
        self.slug = slugify_unicode(name)

    @property
    def project_count(self):
        # TODO: This query is wrong; it returns the total number of projects...
        return Session.object_session(self).query(Project).with_parent(self, 'projects').count()


class PrimeOrganization(db.Model):
    """
    Points to the organization running this software. This organization cannot be deleted, and is edited in the admin
    interface, rather than the main organization interface.
    """
    __tablename__ = 'prime_organization'

    # TODO: Add contstraints to allow one and only one prime organization.
    org_id = db.Column(db.Integer, ForeignKey('organizations.id'), primary_key=True)
    organization = db.relationship('Organization', backref=db.backref('is_prime', uselist=False))


class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, index=True)
    slug = db.Column(db.String, nullable=False, index=True)
    created = db.Column(db.DateTime, nullable=False, index=True)
    modified = db.Column(db.DateTime, index=True)
    published = db.Column(db.DateTime, index=True)
    revoked = db.Column(db.DateTime, index=True)
    bounding_box = db.Column(Geography('MULTIPOINT'))

    organizations = db.relationship('OrganizationsProjects', backref=db.backref('project'))
    linked_projects = db.relationship('Project', secondary=projects_projects,
                                      primaryjoin=projects_projects.c.project2_id == id,
                                      secondaryjoin=projects_projects.c.project1_id == id,
                                      backref='linked_from_projects')

    def __init__(self, name):
        self.name = name
        self.created = datetime.utcnow()
        self.slug = slugify_unicode(name)

    @property
    def created_slug(self):
        return str(self.created.date())

    @property
    def publish_status(self):
        if self.published is None:
            return 'unpublished'

        if self.revoked is None or self.published > self.revoked:
            return 'published'

        if self.published <= self.revoked:
            return 'revoked'

        return 'unknown'

    @property
    def latest_change(self):
        if self.published is None and self.revoked is None:
            return None

        if self.revoked is None:
            return self.published

        return self.revoked if self.published <= self.revoked else self.published