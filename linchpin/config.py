# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT


class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://merseyviking@localhost/linchpin'
    CRSF_ENABLED = True
    LOG_LEVEL = 'INFO'


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql:///linchpin'


class DevelopmentConfig(Config):
    DEBUG = True
    LOG_LEVEL = 'DEBUG'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    LOG_LEVEL = 'DEBUG'