Linchpin
========

Linchpin is a geospatial database and front-end, with document management and publication capabilities, designed with
archaeologists in mind.

Add-on modules
--------------

 * PAST: http://folk.uio.no/ohammer/past/
 * Harris matrices
   * https://gitorious.org/harris-matrix
   * https://code.google.com/p/hmatrix/
 * 3D maps? https://github.com/tangrams/tangram
 * Time series? http://influxdb.com
 * Others: http://www.findbestopensource.com/product/hmatrix

Ideas
-----

 * Google maps for project overview, and projects by organization. Maybe have two maps side-by-side, where one is the extent of the project, and the other is zoomed out n-levels to give context.
 * Versioning: When a record is published, a snapshot of the state of the record is taken so earlier records can be retreived and the DB can stay consistent with the publication data.

To do
-----

 * Use breadcrumbs instead of flat sidebar navigation.
 
Document management
-------------------

 * http://invenio-software.org/
 * http://www.mayan-edms.com/
 * https://www.google.co.uk/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=python%20document%20management%20server
