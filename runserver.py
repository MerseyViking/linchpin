# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from linchpin import app
import logging

if __name__ == '__main__':
    app.run()
    logging.info('Terminating application "{}".'.format(app.name))