"""Add password change field to user.

Revision ID: 35c58945b5d
Revises: 12aa1689b57
Create Date: 2015-02-16 00:45:32.276163

"""

# revision identifiers, used by Alembic.
revision = '35c58945b5d'
down_revision = '12aa1689b57'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('users', sa.Column('need_password_change', sa.Boolean, nullable=False, server_default='false'))


def downgrade():
    op.drop_column('users', 'need_password_change')
