"""create users table

Revision ID: 12aa1689b57
Revises:
Create Date: 2015-02-12 17:56:55.685344

"""

# revision identifiers, used by Alembic.
revision = '12aa1689b57'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('users',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('name', sa.String, nullable=False),
                    sa.Column('email', sa.String, nullable=False),
                    sa.Column('password', sa.String, nullable=False))


def downgrade():
    op.drop_table('users')
