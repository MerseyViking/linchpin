"""User roles.

Revision ID: 5a05b011f23
Revises: 35c58945b5d
Create Date: 2015-02-16 09:54:29.465768

"""

# revision identifiers, used by Alembic.
revision = '5a05b011f23'
down_revision = '35c58945b5d'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('roles',
                    sa.Column('id', sa.Integer, primary_key=True),
                    sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id'), index=True),
                    sa.Column('role', sa.Enum('administrator', 'superuser', 'create', 'delete', 'view', 'publish',
                                              name='role_type')))


def downgrade():
    op.drop_table('roles')
