# Copyright (c) 2015 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from flask.ext.testing import TestCase

from linchpin import app, db


class MyTest(TestCase):
    def create_app(self):
        return app

    def setUp(self):
        db.create_all()

    def test_summat(self):
        assert True

    def tearDown(self):
        db.session.remove()
        db.drop_all()